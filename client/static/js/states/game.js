// @TODO Replace this code with tank game code.

"use strict"

let TankGame = function(game) {
}

TankGame.prototype = {
	// Variables.
	land: undefined,
	
	tanksList: {},

	shadow: undefined,
	tank: undefined,
	turret: undefined,
		
	enemies: undefined,
	bullets: undefined,
	enemiesTotal: 0,
	enemiesAlive: 0,
	explosions: undefined,
		
	currentSpeed: 0,
	cursors: undefined,
		
	bullets: undefined,
	fireRate: 100,
	nextFire: 0,
	
	player: undefined,
	
	eurecaClient: undefined,
	eurecaServer: undefined,
  serverReady: false,
  myId: 0,

	// Init function. Cariables and eureca connect.
	init: function() {
	  
	  let that = this;
	  // Connect eureca.io.
	  //note we don't need "allow" parameter here
    this.eurecaClient = new Eureca.Client();
    
    //eureca client cannot issue remote calls before ready event.
    //we need to wait for it
    this.eurecaClient.ready(function(proxy) {		
		  that.eurecaServer = proxy;
	  });
    
    //methods defined under "exports" namespace become available in the server side
  	this.eurecaClient.exports.setId = (function(id) {
  		//create() is moved here to make sure nothing is created before uniq id assignation
  		this.myId = id;
      this.create();
  		this.eurecaServer.handshake();
      this.serverReady = true;
  	}).bind(this);
  	
  	// Kill tank.
  	this.eurecaClient.exports.kill = (function(id) {	
  		if (this.tanksList[id]) {
  			this.tanksList[id].kill();
  			delete this.tanksList[id];
  			console.log('killing ', id, this.tanksList[id]);
  		}
  	}).bind(this);
  	
  	// Spawn enemy.
  	this.eurecaClient.exports.spawnEnemy = (function(i, x, y) {
  		
  		if (i == this.myId) return; //this is me
  		
  		console.log('SPAWN', i, x, y);
  		let tank = new Tank(i, this.game);
  		tank.x = x;
      tank.y = y;
  		this.tanksList[i] = tank;
  	}).bind(this);
  	
  	// Update states.
  	this.eurecaClient.exports.updateState = (function(id, state) {
  		if (this.tanksList[id]) {
  		  console.log(state.x);
  			this.tanksList[id].cursor = state;
  			this.tanksList[id].tank.x = state.x;
  			this.tanksList[id].tank.y = state.y;
  			this.tanksList[id].tank.angle = state.angle;
  			this.tanksList[id].turret.rotation = state.rotation;
  			this.tanksList[id].update();
  		}
  	}).bind(this);
	},
	
	// Create function.
	create: function() {
	  
		//  Resize our game world to be a 2000 x 2000 square
    this.game.world.setBounds(-1000, -1000, 2000, 2000);
    this.game.stage.disableVisibilityChange = true;

    //  Our tiled scrolling background
    this.land = this.game.add.tileSprite(0, 0, 800, 600, 'earth');
    this.land.fixedToCamera = true;
    
    this.player = new Tank(this.myId, this.game);
    
    this.tanksList[this.myId] = this.player;
    this.tank = this.player.tank;
    this.turret = this.player.turret;
    this.tank.x = 0;
    this.tank.y = 0;
    
    this.bullets = this.player.bullets;
    this.shadow = this.player.shadow;

    //  Explosion pool
    this.explosions = this.game.add.group();

    for (let i = 0; i < 10; i++) {
      let explosionAnimation = this.explosions.create(0, 0, 'kaboom', [0], false);
      explosionAnimation.anchor.setTo(0.5, 0.5);
      explosionAnimation.animations.add('kaboom');
    }

    this.tank.bringToTop();
    this.turret.bringToTop();

    this.game.camera.follow(this.tank);
    this.game.camera.deadzone = new Phaser.Rectangle(150, 150, 500, 300);
    this.game.camera.focusOnXY(0, 0);

    this.cursors = this.game.input.keyboard.createCursorKeys();
	},
	
	update: function() {

	  //do not update if client not ready
	  if (!this.serverReady) return;
	  
  	this.player.input.left = this.cursors.left.isDown;
  	this.player.input.right = this.cursors.right.isDown;
  	this.player.input.up = this.cursors.up.isDown;
  	this.player.input.fire = this.game.input.activePointer.isDown;
  	this.player.input.tx = this.game.input.x + this.game.camera.x;
  	this.player.input.ty = this.game.input.y + this.game.camera.y;

  	this.turret.rotation = this.game.physics.arcade.angleToPointer(this.turret);	
    this.land.tilePosition.x = -this.game.camera.x;
    this.land.tilePosition.y = -this.game.camera.y;

    for (let i in this.tanksList) {
  		if (!this.tanksList[i]) continue;
  		let curBullets = this.tanksList[i].bullets;
  		let curTank = this.tanksList[i].tank;
  		
  		for (let j in this.tanksList) {
  			if (!this.tanksList[j]) continue;
  			if (j != i) {
  			
  				let targetTank = this.tanksList[j].tank;
  				
  				this.game.physics.arcade.overlap(curBullets, targetTank, this.bulletHitPlayer, null, this);
  			
  			}
  			
  			if (this.tanksList[j].alive) {
  				this.tanksList[j].update(this.myId, this.eurecaServer);
  			}			
  		}
    }
	},
	
	render: function() {
	},
	
	bulletHitPlayer: function (tank, bullet) {
    bullet.kill();
	}
}