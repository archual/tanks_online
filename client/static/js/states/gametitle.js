"use strict"

let gameTitle = function(game){}

gameTitle.prototype = {
	
  	create: function(){
  	 	//  Our tiled scrolling background
    	let land = game.add.tileSprite(0, 0, 800, 600, 'earth');
    	land.fixedToCamera = true;
    	
		let gameTitle = this.game.add.sprite(GAME_WIDTH / 2, 160, "logo");
		gameTitle.anchor.setTo(0.5,0.5);
		let playButton = this.game.add.button(GAME_WIDTH / 2, 320, "play", this.playTheGame,this);
		playButton.anchor.setTo(0.5,0.5);
		

	},
	
	playTheGame: function(){
		this.game.state.start("TheGame");
	}
}