// Server js.
'use strict'

//
let http = require('http');
let path = require('path');
let express = require('express');
let Eureca = require('eureca.io');
//create an instance of EurecaServer
let eurecaServer = new Eureca.Server({allow:['setId', 'spawnEnemy', 'kill', 'updateState']});
let clients = {};
 
let router = express();
let server = http.createServer(router);

// serve static files from the current directory
router.use(express.static(path.resolve(__dirname, 'client')));


//attach eureca.io to our http server
eurecaServer.attach(server);
 
server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  let addr = server.address();
  console.log("Tank's server listening at", addr.address + ":" + addr.port);
});

// Eureca liseteners.
// detect client connection
eurecaServer.onConnect(function (conn) {    
  console.log('New Client id=%s ', conn.id, conn.remoteAddress);
	
	//the getClient method provide a proxy allowing us to call remote client functions
  let remote = eurecaServer.getClient(conn.id);    
	
	//register the client
	clients[conn.id] = {id:conn.id, remote:remote}
	
	//here we call setId (defined in the client side)
	remote.setId(conn.id);	
});
 
 
// detect client disconnection
eurecaServer.onDisconnect(function (conn) {    
  console.log('Client disconnected ', conn.id);
  
  let removeId = clients[conn.id].id;
	
	delete clients[conn.id];
	
	for (let c in clients) {
		let remote = clients[c].remote;
		
		//here we call kill() method defined in the client side
		remote.kill(conn.id);
	}	
});

// Handshake method.
eurecaServer.exports.handshake = function() {
	//let conn = this.connection;
	for (let c in clients) {
		let remote = clients[c].remote;
		for (let cc in clients)	{		
		  //send latest known position
			let x = clients[cc].laststate ? clients[cc].laststate.x:  0;
			let y = clients[cc].laststate ? clients[cc].laststate.y:  0;
 
			remote.spawnEnemy(clients[cc].id, x, y);		
		}
	}
}

// handleKeys method.
eurecaServer.exports.handleKeys = function (keys) {
	let conn = this.connection;
	let updatedClient = clients[conn.id];
	
	for (let c in clients)
	{
		let remote = clients[c].remote;
		remote.updateState(updatedClient.id, keys);
		
		//keep last known state so we can send it to new connected clients
		clients[c].laststate = keys;
	}
}